# SOAP CRUD Service

# Build
```
mvn clean install
```

# Run
```
docker-compose up -d
java -jar target/soap-crud-service.jar
```

package org.iborodin.dp;

import org.iborodin.dp.endpoint.ProjectEndpoint;
import org.iborodin.dp.endpoint.TaskEndpoint;
import org.iborodin.dp.service.ProjectService;
import org.iborodin.dp.service.TaskService;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.ws.Endpoint;

/**
 * Hello world!
 */
public class App {

    final EntityManagerFactory factory;
    final TaskService taskService;
    final ProjectService projectService;
    final TaskEndpoint taskEndpoint;
    final ProjectEndpoint projectEndpoint;

    public App(final String persistenceUnitName) {
        factory = Persistence.createEntityManagerFactory(persistenceUnitName);
        taskService = new TaskService(factory);
        projectService = new ProjectService(factory);
        taskEndpoint = new TaskEndpoint(taskService);
        projectEndpoint = new ProjectEndpoint(projectService);
    }

    public void start(final String urlBase) {

        Endpoint.publish(urlBase + projectEndpoint.URL, projectEndpoint);
        System.out.println(urlBase + projectEndpoint.URL);
        Endpoint.publish(urlBase + taskEndpoint.URL, taskEndpoint);
        System.out.println(urlBase + taskEndpoint.URL);
    }

    public static void main(String[] args) {

        final App app = new App("entityManager");
        app.start("http://127.0.0.1:8080");
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }
}

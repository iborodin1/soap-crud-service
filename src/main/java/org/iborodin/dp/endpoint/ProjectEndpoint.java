package org.iborodin.dp.endpoint;

import org.iborodin.dp.entity.Project;
import org.iborodin.dp.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    public static final String URL = "/ProjectEndpoint?wsdl";

    private ProjectService ProjectService;

    public ProjectEndpoint(final ProjectService ProjectService) {
        this.ProjectService = ProjectService;
    }

    @WebMethod
    public List<Project> findAllProject() {
        return ProjectService.findAll();
    }

    @WebMethod
    public Project findOneByIdProject(@WebParam(name = "id") long id) {
        return ProjectService.findOneById(id);
    }

    @WebMethod
    public Project createProject(@WebParam(name = "name") String name) {
        return ProjectService.create(name);
    }

    @WebMethod
    public Project updateProject(@WebParam(name = "Project") Project Project) {
        return ProjectService.update(Project);
    }

    @WebMethod
    public Project removeOneByIdProject(@WebParam(name = "id") long id) {
        return ProjectService.removeOneById(id);
    }

    @WebMethod
    public void removeAllProject() {
        ProjectService.removeAll();
    }

}


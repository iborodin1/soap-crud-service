package org.iborodin.dp.endpoint;

import org.iborodin.dp.entity.Task;
import org.iborodin.dp.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint {

    public final String URL = "/TaskEndpoint?wsdl";

    private TaskService taskService;

    public TaskEndpoint(final TaskService taskService) {
        this.taskService = taskService;
    }

    @WebMethod
    public List<Task> findAllTask() {
        return taskService.findAll();
    }

    @WebMethod
    public Task findOneByIdTask(@WebParam(name = "id") long id) {
        return taskService.findOneById(id);
    }

    @WebMethod
    public Task createTask(@WebParam(name = "name") String name) {
        return taskService.create(name);
    }

    @WebMethod
    public Task updateTask(@WebParam(name = "task") Task task) {
        return taskService.update(task);
    }

    @WebMethod
    public Task removeOneByIdTask(@WebParam(name = "id") long id) {
        return taskService.removeOneById(id);
    }

    @WebMethod
    public void removeAllTask() {
        taskService.removeAll();
    }

}

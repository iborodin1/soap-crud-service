package org.iborodin.dp.repository;

import javax.persistence.EntityManager;
import java.util.List;

public class BasicRepository<T> {

    private final EntityManager entityManager;
    private final Class<T> typeParameterClass;

    public BasicRepository(final EntityManager entityManager, final Class<T> typeParameterClass) {
        this.entityManager = entityManager;
        this.typeParameterClass = typeParameterClass;
    }

    public List<T> findAll() {
        return entityManager
                .createQuery("FROM " + typeParameterClass.getName(), typeParameterClass)
                .getResultList();
    }

    public T findOneById(long id) {
        return entityManager.find(typeParameterClass, id);
    }

    public T create(final T value) {
        entityManager.persist(value);
        return value;
    }

    public T update(final T value) {
        return entityManager.merge(value);
    }

    public T removeOneById(final long id) {
        final T value = findOneById(id);
        if (value == null) return null;
        entityManager.remove(value);
        return value;
    }

    public void removeAll() {
        entityManager.createQuery("DELETE FROM " + typeParameterClass.getName()).executeUpdate();
    }


}

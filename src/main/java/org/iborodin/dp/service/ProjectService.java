package org.iborodin.dp.service;

import org.iborodin.dp.entity.Project;
import org.iborodin.dp.repository.BasicRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class ProjectService {
    
    private final EntityManagerFactory entityManagerFactory;

    public ProjectService(final EntityManagerFactory factory)
    {
        this.entityManagerFactory = factory;
    }

    private BasicRepository<Project> getProjectRepository(final EntityManager entityManager) {
        return new BasicRepository<>(entityManager, Project.class);
    }

    public List<Project> findAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getProjectRepository(entityManager).findAll();
        } finally {
            entityManager.close();
        }
    }

    public Project findOneById(final long id) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getProjectRepository(entityManager).findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Project createProject = new Project();
            createProject.setName(name);
            final Project Project = getProjectRepository(entityManager).create(createProject);
            entityManager.getTransaction().commit();
            return Project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Project update(final Project Project) {
        if (Project == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getProjectRepository(entityManager).update(Project);
            entityManager.getTransaction().commit();
            return Project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Project removeOneById(final long id) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Project Project = getProjectRepository(entityManager).removeOneById(id);
            entityManager.getTransaction().commit();
            return Project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public void removeAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getProjectRepository(entityManager).removeAll();
            entityManager.getTransaction().commit();
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

}

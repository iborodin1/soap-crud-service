package org.iborodin.dp.service;

import org.iborodin.dp.entity.Task;
import org.iborodin.dp.repository.BasicRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class TaskService {

    private final EntityManagerFactory entityManagerFactory;

    public TaskService(final EntityManagerFactory factory)
    {
        this.entityManagerFactory = factory;
    }

    private BasicRepository<Task> getTaskRepository(final EntityManager entityManager) {
        return new BasicRepository<>(entityManager, Task.class);
    }

    public List<Task> findAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getTaskRepository(entityManager).findAll();
        } finally {
            entityManager.close();
        }
    }

    public Task findOneById(final long id) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getTaskRepository(entityManager).findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Task createTask = new Task();
            createTask.setName(name);
            final Task task = getTaskRepository(entityManager).create(createTask);
            entityManager.getTransaction().commit();
            return task;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Task update(final Task task) {
        if (task == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Task removeOneById(final long id) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Task task = getTaskRepository(entityManager).removeOneById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public void removeAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).removeAll();
            entityManager.getTransaction().commit();
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }


}

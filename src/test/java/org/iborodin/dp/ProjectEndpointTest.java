package org.iborodin.dp;

import lombok.val;
import org.iborodin.dp.generated.Project;
import org.iborodin.dp.generated.ProjectEndpointService;
import org.junit.Assert;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class ProjectEndpointTest {

    private final String urlBase = "http://127.0.0.1:9876";
    private final App app;

    public ProjectEndpointTest() {
        app = new App("testEntityManager");
        app.start(urlBase);
    }

    @Test
    public void testCreate() throws MalformedURLException {
        val service = new ProjectEndpointService(new URL(urlBase + org.iborodin.dp.endpoint.ProjectEndpoint.URL));

        final Project created = service.getProjectEndpointPort().createProject("test");

        Assert.assertNotNull(created);
        Assert.assertEquals("test", created.getName());
        Assert.assertTrue(created.getId() > 0);
    }
}

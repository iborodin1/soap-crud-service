package org.iborodin.dp;

import org.iborodin.dp.entity.Project;
import org.iborodin.dp.service.ProjectService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class ProjectServiceTest {

    final EntityManagerFactory factory = Persistence.createEntityManagerFactory("testEntityManager");
    final ProjectService projectService = new ProjectService(factory);

    @Before
    public void clearProjects() {
        projectService.removeAll();
    }

    @Test
    public void testFindAllOnEmptyDB() {
        final List<Project> result = projectService.findAll();

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void testCreateOnEmptyDB() {
        final String projectName = "test";

        final Project result = projectService.create(projectName);

        Assert.assertNotNull(result);
        Assert.assertEquals(projectName, result.getName());
        Assert.assertTrue(result.getId() > 0);

        final List<Project> all = projectService.findAll();
        Assert.assertFalse(all.isEmpty());
        Assert.assertEquals(1, all.size());
        Assert.assertEquals(projectName, all.get(0).getName());
    }

    @Test
    public void testCreateNoName() {
        final Project result = projectService.create(null);

        Assert.assertNull(result);
    }
}
